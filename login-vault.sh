#!/bin/bash

###############################################################################
# login to vault
# script forked from git@code.stanford.edu:et-public/cloud-framework.git
# Add token renewal if already logged in so this can be used as a part of a
# agent service unit when approle method is used
###############################################################################

# requires
hash jq 2>&1 /dev/null || { echo "Error: jq not installed " >&2; exit 1; }
hash vault 2>&1 /dev/null || { echo "Error: vault not installed " >&2; exit 1; }


set +u # optional vars
export VAULT_ADDR=${VAULT_ADDR:-https://vault.stanford.edu}
export VAULT_AUTH_PATH=${VAULT_AUTH_PATH:-ldap}
export VAULT_AUTH_METHOD=${VAULT_AUTH_METHOD:-ldap}
export VAULT_USER=${VAULT_USER:-$USER}
export VAULT_ROLE_ID=${VAULT_ROLE_ID}
set -u

if [ -z ${VAULT_USER} ]; then
    echo -e "Error: USER variable undefined. \nusage: USER=my_username ${0}"
    exit 1
fi

echo "VAULT SERVER: $VAULT_ADDR"

# try to renew

now=$(date)
renew_response=$(vault token renew -format=json 2>&1)

if ! echo $renew_response | grep -E -q "Error|missing client token"; then

    duration=$(echo $renew_response | jq -r .auth.lease_duration)

    # use date/gdate for linux/mac
    expiration=$(date -d "$now +$duration seconds" 2>/dev/null) || {
        expiration=$(gdate -d "$now +$duration seconds")
    }

    if vault token lookup > /dev/null 2>&1; then
        echo "Vault token lease renewed for $duration seconds. Expires $expiration"
    else
        echo "Unable to renew token lease."
    fi
    exit 0

fi

if ! [ -z $VAULT_ROLE_ID ]; then
    echo "attempting to login in w/ approle provided in env vars VAULT_ROLE_ID & VAULT_SECRET_ID"
    vault write -format json auth/approle/login \
        role_id=$VAULT_ROLE_ID \
        secret_id=$VAULT_SECRET_ID \
        | jq -er .auth.client_token > ~/.vault-token
fi

if ! vault token lookup > /dev/null 2>&1; then
    if ! [ -z $VAULT_ROLE_ID ]; then
        >&2 "ERROR: VAULT_TOKEN does not exist or is not valid" && false
    fi

    echo "Please login VAULT as vault user ${VAULT_USER} with DUO device ready:"
    vault login -method=${VAULT_AUTH_METHOD} -path=${VAULT_AUTH_PATH} username=${VAULT_USER}
fi


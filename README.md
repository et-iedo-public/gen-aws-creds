# generate a time-bound aws credential

Vault dynamically generates a short-lived aws credential that is automatically revoked at expiration.

```bash
    # usage

    source ./gen-aws-creds.sh <aws_mount> <iam_role>

    required parameters:

    <aws_mount> the aws secret engine path in Vault
    <iam_role>  the name of the IAM role to be attached to the generated credentials
```

### Note: 

Each aws account requires its own secret engine mounted in Vault.

You must be logged-in to vault and have set the environment variable `VAULT_ADDR=https://vault.stanford.edu`

## Demo

```console

$ # try to generate a credential without a Vault token

$ source ./gen-aws-creds.sh aws-iedo-dev IAMReadOnlyAccess

Missing client token. Login to Vault.

$ # first we need to authenticate with vault using our LDAP credentials:

$ login-vault.sh

VAULT SERVER: https://vault.stanford.edu
Please login VAULT as vault user idouglas with DUO device ready:
Password (will be hidden):

Success! You are now authenticated. The token information displayed below
is already stored in the token helper. You do NOT need to run "vault login"
again. Future Vault requests will automatically use this token.

$ # notice the current environment variables:

$ printenv | grep AWS_

AWS_DEFAULT_REGION=us-west-2

$ # so far we only have AWS_DEFAULT_REGION

$ # now that we have our vault token, get aws creds:

$ source ./gen-aws-creds.sh aws-iedo-dev IAMReadOnlyAccess

no credentials stashed in vault
generated AWS credentials:
lease_id: aws-iedo-dev/creds/IAMReadOnlyAccess/OWCWC0n1Xpp9UIXYxpqpjCkc
lease_expiration: 02/16/2019 01:58:44

$ # we now have environment variables that allow us to interact with AWS:

$ printenv | grep AWS_

AWS_DEFAULT_REGION=us-west-2
AWS_SECRET_ACCESS_KEY=9876543210abcdefgHIJkl/ZYXWVUTSrqponm+2a
AWS_ACCESS_KEY_ID=AKIAIPWQN5D23Y2PK6IQ

$ # let's take a look at the IAM users in AWS:

$ aws iam list-users | grep vault

            "UserName": "vault-ldap-idouglas-IAMReadOnlyAccess-1550300325-5784",
            "Arn": "arn:aws:iam::890670543869:user/vault-ldap-idouglas-IAMReadOnlyAccess-1550300325-5784",


            "UserName": "vault-provisioner",
            "Arn": "arn:aws:iam::890670543869:user/vault-provisioner"

$ # we see 2 credentials, vault-provisioner, which belongs to vault,
$ # and vault-ldap-idouglas-<IAM_ROLE>-<UUID> which was just generated and is in use by this command

$ # let's try to generate new IAM credentials
$ # -- since the credential previously generated hasn't expired, no new credential is made:

$ source ./gen-aws-creds.sh aws-iedo-dev IAMReadOnlyAccess

Found cached credentials:
lease_id = aws-iedo-dev/creds/IAMReadOnlyAccess/OWCWC0n1Xpp9UIXYxpqpjCkc
using cached credentials expiring on 02/16/2019 01:58:44

$ # let's overwrite one of the environment variables used and try that again:

$ export AWS_ACCESS_KEY_ID="none"

$ # trying to interact with AWS will result in an access error:

$ aws iam list-users | grep vault

An error occurred (InvalidClientTokenId) when calling the ListUsers operation: The security token included in the request is invalid.

$ # let's get the cached credentials and try again:

$ source ./gen-aws-creds.sh aws-iedo-dev IAMReadOnlyAccess

Found cached credentials:
lease_id = aws-iedo-dev/creds/IAMReadOnlyAccess/OWCWC0n1Xpp9UIXYxpqpjCkc
WARNING: cached credentials do not match current environment.

Use cached credentials (Y) or get new (ENTER): Y
using cached credentials expiring on 02/16/2019 01:58:44

$ # we can now interact with AWS with our previously generated creds:

$ aws iam list-users | grep vault

            "UserName": "vault-ldap-idouglas-IAMReadOnlyAccess-1550300325-5784",
            "Arn": "arn:aws:iam::890670543869:user/

            vault-ldap-idouglas-IAMReadOnlyAccess-1550300325-5784",
            "UserName": "vault-provisioner",
            "Arn": "arn:aws:iam::890670543869:user/vault-provisioner",

$ # Only the single dynamic iam user has been created

$ # Vault will remove this account from AWS at the expiration time

```

## bash or pwsh

#### bash:

![](images/vault-aws-cred2.gif)

#### powershell:

![](images/vault-aws-cred.gif)

### see also

For Vault AWS secrets engine administrators, see examples of how to inject IAM policies into vault using Terraform:

[AWS IAM Policy to Vault (terraform) - Single ARN reference](https://code.stanford.edu/snippets/61)

[AWS IAM Policy to Vault (terraform) - Multiple ARN references](https://code.stanford.edu/snippets/65)

### quick start

login to vault and generate an AWS cred:

```console
$ git clone https://code.stanford.edu/et-iedo-public/gen-aws-creds.git && cd gen-aws-creds
$ ./login-vault.sh
$ source ./gen-aws-creds.sh <aws_mount> <iam_role>
```

#!/bin/bash
#set -x
# Generates time-bound AWS credentials from vault secrets engine
# stashes credentials in the vault token cubbyhole to avoid generating 
# new credentials when previously generated creds have not expired

# set variable if run directly and not sourced
(return 2>/dev/null) && sourced=1 || sourced=0

usage(){
    echo
    echo "WARNING: this script modifies environment variables and thus must be 'sourced' --"
    echo "Do not call directly."
    echo
    echo "Generate time-bound AWS credentials"
    echo
    echo "usage:"
    echo "source ${0} <aws_mount> <iam_role>"
    echo
    echo "required parameters:"
    echo "<aws_mount> the aws secret engine path in Vault"
    echo "<iam_role>  the name of the IAM role to be attached to the generated credentials"
}

if [[ $sourced -eq 0 && -z has_run ]]; then
    usage
    exit
fi

if [[ $# -lt 2 ]] ; then
    usage
    sleep 4; exit 1
fi

aws_mount=$1
iam_role=$2
has_run=true

# nb:
# this script MUST be sourced, not run directly, because
# we need to write global environment variables from a child scope (not possible in bash)
# because of this, flow control in the main block uses a branching structure instead of exit(s) because 'exit' will kill parent shell

if [ ! $(which vault) ]; then
    echo "vault not installed. Exiting." >&2
    sleep 4; exit 1
fi

if [ ! $(which jq) ]; then
    echo "jq not installed. Exiting." >&2
    sleep 4; exit 1
fi

# ensure logged into vault
export VAULT_ADDR=${VAULT_ADDR:-https://vault.stanford.edu}
token_response=$(vault token lookup -format=json 2>&1)

if [[ ${token_response} =~ "Error looking up token" ]]; then
    echo "Missing client token. Login to Vault."
    vault_loggedon='FALSE'
else
    vault_loggedon='TRUE'
fi

if [[ $vault_loggedon = 'TRUE' ]]; then

    # check for stashed creds
    stash=$(vault read -format=json cubbyhole/aws_creds 2>&1)

    if [[ ! ${stash} =~ ^'Error'|^'No value' ]]; then

        echo "Found cached credentials:"
        # set key values in json to shell vars
        eval $(echo $stash | jq -r '.data | to_entries | .[] | .key + "=\"" + .value + "\""')

        echo "lease_id = $lease_id"

        # use date or gdate for linux/mac interop
        # gdate on mac: brew install coreutils
        now=$(date +%s 2>/dev/null) && expiration_u=$(date -d "$expiration" +%s 2>/dev/null) || {
            now=$(gdate +%s 2>/dev/null) && expiration_u=$(gdate -d "$expiration" +%s 2>/dev/null)  || 
            now=$(date +%s 2>/dev/null) && expiration_u=$(date -j -f "%m/%d/%Y %H:%M:%S"  "$expiration" +%s 2>/dev/null)
        }

        # check if stashed creds have not expired
        if [ $now -lt $expiration_u ]; then 
            # creds match environment
            if [[ ( $AWS_ACCESS_KEY_ID == $access_key_id ) && ( $AWS_SECRET_ACCESS_KEY == $secret_access_key ) ]]; then
                echo "using cached credentials expiring on $expiration"
                creds_get_new=FALSE
            else
                # creds don't match environment
                echo "WARNING: cached credentials do not match current environment."
                echo -n "Use cached credentials (Y) or get new (ENTER): "
                read use_creds
                if [[ $use_creds == 'Y' ]]; then
                    export AWS_ACCESS_KEY_ID=$access_key_id
                    export AWS_SECRET_ACCESS_KEY=$secret_access_key
                    echo "using cached credentials expiring on $expiration"
                    #exit 0;
                    creds_get_new=FALSE
                else
                    creds_get_new=TRUE
                fi
            fi
        else
            echo "Credentials expired on $expiration"
            creds_get_new=TRUE
        fi
    else
        creds_get_new=TRUE
        echo "no credentials stashed in vault"
    fi # end check stash

fi # end if vault_logged_on

request_time=$(date)

if [[ $creds_get_new == 'TRUE' ]]; then
    # generate creds
    echo "generating credentials from $aws_mount/creds/$iam_role"
    aws_cred=$(vault read -format=json $aws_mount/creds/$iam_role)

    if [[ ( $aws_cred =~ ^'No value' ) || ( -z $aws_cred ) ]]; then
        echo "Unable to retrieve credentials from $aws_mount/creds/$iam_role"
        #exit 1
        creds_generated=FALSE
    else
        creds_generated=TRUE
    fi
fi

if [[ ( $creds_generated == 'TRUE' ) && ( $creds_get_new == 'TRUE' ) ]]; then

    # set key values in json to shell vars: access_key, secret_key
    eval $(echo $aws_cred | jq -r '.data | to_entries | .[] | .key + "=\"" + .value + "\""')

    # set key values in json to shell vars: lease_id, lease_duration
    eval $(echo $aws_cred | jq -r '. | to_entries | .[] | "\(.key)" + "=\"" + "\(.value)" + "\""')

    # calc lease expiration time
    # use date or gdate for linux/mac interop
    expiration=$(date --date="$request_time+ $lease_duration seconds" '+%m/%d/%Y %T' 2>/dev/null) || 
        expiration=$(gdate --date="$request_time+ $lease_duration seconds" '+%m/%d/%Y %T' 2>/dev/null)  ||
        expiration=$(date -j -f "%a %b %d %T %Z %Y"  -v+${lease_duration}S "$request_time" '+%m/%d/%Y %T' 2>/dev/null)

    if [[ ( $access_key ) && ( $secret_key ) ]]; then
        export AWS_SECRET_ACCESS_KEY=$secret_key
        export AWS_ACCESS_KEY_ID=$access_key
        echo "generated AWS credentials:"
        echo "lease_id: $lease_id"
        echo "lease_expiration: $expiration"
    else
        echo "Unable to generate AWS credentials."
        #exit 1
        creds_generated=FALSE
    fi

fi # end get creds

if [[ $creds_generated == 'TRUE' ]]; then
    
    # stash creds
    stash_response=$(vault write cubbyhole/aws_creds access_key_id="$access_key" secret_access_key="$secret_key" expiration="$expiration" lease_id="$lease_id" 2>&1)

    if [[ ! $stash_response =~ ^'Success' ]]; then
        echo "WARNING: Unable to stash generated AWS credentials."
    fi

fi # end stash creds
